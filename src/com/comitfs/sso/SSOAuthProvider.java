package com.comitfs.sso;

import java.util.ArrayList;
import java.util.List;

import org.jivesoftware.openfire.auth.AuthProvider;
import org.jivesoftware.openfire.auth.ConnectionException;
import org.jivesoftware.openfire.auth.InternalUnauthenticatedException;
import org.jivesoftware.openfire.auth.UnauthorizedException;
import org.jivesoftware.openfire.group.Group;
import org.jivesoftware.openfire.group.GroupManager;
import org.jivesoftware.openfire.http.HttpBindServlet;
import org.jivesoftware.openfire.user.User;
import org.jivesoftware.openfire.user.UserManager;
import org.jivesoftware.openfire.user.UserNotFoundException;
import org.jivesoftware.util.JiveGlobals;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SSOAuthProvider implements AuthProvider{
	
	private static final Logger Log = LoggerFactory.getLogger(SSOAuthProvider.class);
	
	
	public SSOAuthProvider() {
		super();
		String contextName = JiveGlobals.getProperty("cas.auth.siteminder.context.name", "/http-sso-bind");
		 AddSSOContext.getInstance().createSSOContextHandler(contextName);
	}

	@Override
	public void authenticate(String userName, String password)
			throws UnauthorizedException, ConnectionException, InternalUnauthenticatedException {
		String user = userName.split("@")[0];

		boolean isOK = false;

		Log.info("User details SSO"+ user);

		if(user != null) {
			

			if(!isOK && JiveGlobals.getBooleanProperty("plugin.cas.rest.api.nonauth.enabled", false)) {
				isOK = true;
				return;
			}
			
			if(AddSSOContext.getInstance().getUserSessionMap().containsKey(user) ){
				isOK = autheticateUser(user);
			} 
			
			if(!isOK && (JiveGlobals.getBooleanProperty("plugin.cas.rest.api.sso.enabled", false) 
					  || JiveGlobals.getBooleanProperty("plugin.cas.rest.api.nonauth.enabled", false))) {
				
				String userid = JiveGlobals.getProperty("plugin.cas.rest.api.local.session.user");
				
				if(userid!= null && userid.equals(userName)) {
					User usr;
					try {
						usr = UserManager.getInstance().getUser(userName.trim());
						if(usr.getProperties().containsKey("restSecret") && usr.getProperties().get("restSecret").equalsIgnoreCase(password)) {
			        		isOK = true;
			        	}else {
			        		isOK = false;
			        		Log.error("User " + userName + " authentication failed");
			        	}
					} catch (UserNotFoundException e) {
						isOK = false;
						Log.error("User " + userName + " is not registered in openfire");
					}
		        	
				}else if(!isOK && JiveGlobals.getBooleanProperty("plugin.cas.rest.api.nonauth.enabled", false)) {
					isOK = true;
				}
			}
			
			
			if(!isOK) {
				throw new UnauthorizedException();
			}
		}


	}
	
	 private Boolean autheticateUser(String userName) {
	    	Boolean result = Boolean.FALSE;
	    	 User user;
			try {
				user = UserManager.getInstance().getUser(userName.trim());
				List<User> users = new ArrayList(UserManager.getInstance().getUsers());
				
				List<Group> groups = new ArrayList<Group>(GroupManager.getInstance().getGroups(user));
				for(Group group : groups) {
					if(group.getName().equalsIgnoreCase(JiveGlobals.getProperty("cas.auth.siteminder.cas.client.group", "itrader-dev"))) {
						result = Boolean.TRUE;		
					}
				}
			} catch (UserNotFoundException e) {
				Log.error("User "+ userName + " is not registered in openfire");
			}
	         return result;
	    }

	@Override
	public void authenticate(String arg0, String arg1, String arg2)
			throws UnauthorizedException, ConnectionException, InternalUnauthenticatedException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getPassword(String arg0) throws UserNotFoundException, UnsupportedOperationException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isDigestSupported() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isPlainSupported() {
		// TODO Auto-generated method stub
		return true;
	}



	@Override
	public void setPassword(String arg0, String arg1) throws UserNotFoundException, UnsupportedOperationException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean supportsPasswordRetrieval() {
		// TODO Auto-generated method stub
		return false;
	}

}
