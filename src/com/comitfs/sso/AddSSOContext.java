package com.comitfs.sso;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.jetty.server.handler.ContextHandlerCollection;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.jivesoftware.openfire.http.HttpBindManager;
import org.jivesoftware.openfire.http.SSOApiServlet;

public class AddSSOContext {
	
	public static final AddSSOContext addSSOContext = new AddSSOContext();
	private Map<String, String> userSessionMap = new HashMap<String, String>();
	
	public Map<String, String> getUserSessionMap() {
		return userSessionMap;
	}


	public static AddSSOContext getInstance() {
		return addSSOContext;
	}

	  public void createSSOContextHandler(String boshPath)
	    {	
		  	ContextHandlerCollection contexts = HttpBindManager.getInstance().getContexts();
	        ServletContextHandler context = new ServletContextHandler(contexts, boshPath, ServletContextHandler.SESSIONS);
//	        final List<ContainerInitializer> initializers = new ArrayList<>();
//	        initializers.add(new ContainerInitializer(new JettyJasperInitializer(), null));
//	        context.setAttribute("org.eclipse.jetty.containerInitializers", initializers);
//	        context.setAttribute(InstanceManager.class.getName(), new SimpleInstanceManager());
	        context.addServlet(new ServletHolder(new SSOApiServlet()),"/*");
	       
	    }
	
}
